<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Persona;
use App\Models\Departamento;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas=Persona::all();
        return view('personas.index')->with('personas',$personas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $persona = new Persona();
        $departamentos=Departamento::all();
        return view('personas.create')->with('persona', $persona)->with('departamentos',$departamentos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request['departamento_id']==0) {
            return redirect()->route('personas.create');
        }

        Persona::create([
                'nombre' => $request['nombre'],
                'apellido' => $request['apellido'],
                'cedula' => $request['cedula'],
                'email' => $request['email'],
                'departamento_id' => $request['departamento_id']
        ]);
        return redirect()->route('personas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $persona = Persona::findOrFail($id);
        $departamentos=Departamento::all();
        return view('personas.edit')->with('persona',$persona)->with('departamentos',$departamentos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $persona = Persona::FindOrFail($id);
        $input = $request->all();
        $persona->fill($input)->save();
        return redirect()->route('personas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $persona = Persona::FindOrFail($request['id']);
        $persona->delete();
        return redirect()->route('personas.index');
    }
}
