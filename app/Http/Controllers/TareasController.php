<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Tarea;
use App\Models\DetalleTarea;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;


class TareasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if ($id==1) {
            return view('tareas.index');
        }else{
            dd('hola');
        }

    }

    public function listaTareas($id)
    {
        if ($id==1) {
            $tareas = Tarea::select('*')->where('emisor', '=', Auth::user()->email)->get();
            return view('tareas.index')->with('tareas',$tareas)->with('opcion',1);
        }else{
            $tareas = Tarea::select('*')->where('receptor', '=', Auth::user()->email)->get();
            return view('tareas.index')->with('tareas',$tareas)->with('opcion',2);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tarea= new Tarea();
        return view('tareas.create')->with('tarea',$tarea);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file("archivo")->getClientOriginalName());
        $aleatorio = str_random(6);
        $nombre = $aleatorio.'-'.$request->file("archivo")->getClientOriginalName();
        $request->file("archivo")->move('archivos',$nombre);

        Tarea::create([
                'emisor'        => Auth::user()->email,
                'receptor'      => $request['receptor'],
                'asunto'        => $request['asunto'],
                'estado'        => 'en_proceso',
                'contenido'     => $request['contenido'],
                'archivo'       => $nombre
        ]);

        $pathToFile="";
        $containfile=false; 
        $redireccionar=false;
        if($request->hasFile('archivo') ){
            if ($request['id_tarea']) {
                $redireccionar=true;
            }
            $containfile=true; 
            $aleatorio = str_random(6);
            $nombre = $aleatorio.'-'.$request->file("archivo")->getClientOriginalName();
            $request->file("archivo")->move('archivos',$nombre);
            $file = $request->file('archivo');
            $pathToFile= public_path()."/archivos/".$nombre;
        }

        if($request['id_tarea'] && !$request->hasFile('archivo')){
            $tarea=Tarea::findOrFail($request['id_tarea']);
            $containfile=true;
            $nombre = $tarea->archivo;
            $pathToFile= public_path()."/archivos/".$nombre;
            $redireccionar=true;
        }
        $destinatario=$request->input("receptor");
        $asunto=$request->input("asunto");
        $contenido=$request->input("contenido");


        $data = array('contenido' => $contenido);
        $r= Mail::send('correos.plantilla_correo', $data, function ($message) use ($asunto,$destinatario,  $containfile,$pathToFile) {
            $message->from('cvgfundeportepzo@gmail.com', 'funi567890');
            $message->to($destinatario)->subject($asunto);
            if($containfile){
                $message->attach($pathToFile);
            }

        });
        
        // $name = Carbon::now().$request->file("archivo")->getClientOriginalName();
        // $request->file("archivo")->move('archivos',$name);
        // dd('terminado');
        // return redirect()->route('tareas.index');

        return redirect()->route('listaTareas',"1");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
        $tarea= Tarea::findOrFail($id);
        $archivo= explode("-", $tarea->archivo);
        $archivo = $archivo[1];
        return view('tareas.editar')->with('tarea', $tarea)->with('archivo',$archivo);
    }

    protected function downloadFile($src){
        if(is_file($src)){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $content_type = finfo_file($finfo, $src);
            finfo_close($finfo);
            $file_name = basename($src).PHP_EOL;
            $size = filesize($src);
            header("Content-Type: $content_type");
            header("Content-Disposition: attachment; filename=$file_name");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: $size");
            readfile($src);
            return true;
        } else{
            return false;
        }
    }

    public function descarga($id)
    {
        if(!$this->downloadFile(public_path()."/archivos/".$id)){
            return redirect()->back();
        }
    }


    public function detalleTareas($id,$opcion)
    {
        $tarea= Tarea::findOrFail($id);
        $archivo= explode("-", $tarea->archivo);
        $archivo = $archivo[1];
        return view('tareas.edit')->with('tarea', $tarea)->with('archivo',$archivo)->with('opcion',$opcion);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         dd($request);
    }

    public function actualizar(Request $request)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $tarea = Tarea::FindOrFail($request['id']);
        $tarea->delete();
        return redirect()->route('listaTareas',$request['opcion']);
    }
}
