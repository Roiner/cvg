<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Tarea;
use Mail;

class CorreoController extends Controller
{
	public function enviarCorreo(Request $request)
	{
		$pathToFile="";
		$containfile=false; 
		$redireccionar=false;
		if($request->hasFile('archivo') ){
			if ($request['id_tarea']) {
				$redireccionar=true;
			}
			$containfile=true; 
			$aleatorio = str_random(6);
			$nombre = $aleatorio.'-'.$request->file("archivo")->getClientOriginalName();
			$request->file("archivo")->move('archivos',$nombre);
			$file = $request->file('archivo');
			$pathToFile= public_path()."/archivos/".$nombre;
		}

		if($request['id_tarea'] && !$request->hasFile('archivo')){
			$tarea=Tarea::findOrFail($request['id_tarea']);
			$containfile=true;
			$nombre = $tarea->archivo;
			$pathToFile= public_path()."/archivos/".$nombre;
			$redireccionar=true;
		}
		$destinatario=$request->input("receptor");
		$asunto=$request->input("asunto");
		$contenido=$request->input("contenido");


		$data = array('contenido' => $contenido);
		$r= Mail::send('correos.plantilla_correo', $data, function ($message) use ($asunto,$destinatario,  $containfile,$pathToFile) {
			$message->from('cvgfundeportepzo@gmail.com', 'funi567890');
			$message->to($destinatario)->subject($asunto);
			if($containfile){
				$message->attach($pathToFile);
			}

		});
		
		if($r){
			if ($redireccionar==true) {
				return redirect()->route('listaTareas',1);
			}   
			return redirect()->route('nuevoCorreo');
		}
		else
		{            
			if ($redireccionar==true) {
				return redirect()->route('listaTareas',1);
			}
			return redirect()->route('nuevoCorreo');
		}
    }

    public function nuevoCorreo(){
		$tarea= new Tarea();
        $archivo= "";
        return view('tareas.edit')->with('tarea', $tarea)->with('archivo',$archivo)->with('opcion',3);
    }

    public function actualizar(Request $request)
    {
    	echo "correo actualizado";
    	dd($request);
    }
}
