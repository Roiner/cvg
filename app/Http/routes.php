<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(Auth::User()){
		return redirect()->route('admin.inicio');
    }else{
		return redirect()->route('auth.login');
    }
});


Route::group(['middleware' => 'auth'],function(){
	Route::get('inicio',['as' => 'admin.inicio', function () {
		return view('inicio');
	}]);

	Route::resource('usuarios','UsuarioController');
	Route::resource('departamentos','DepartamentoController');
	Route::resource('personas','PersonaController');
	Route::resource('tareas','TareasController');
	Route::resource('tareas','TareasController');
	Route::resource('detalles_tareas','Detalle_tareaController');
	Route::get('listaTareas/{id?}','TareasController@listaTareas');
	Route::get('listaTareas/{id?}', [  /*Ruta para mostrar la tabla donde se crea la tarea*/
                'uses' => 'TareasController@listaTareas',
                'as' => 'listaTareas'
                ]);
	Route::get('descarga/{id?}', [  
                'uses' => 'TareasController@descarga',
                'as' => 'descargarTarea'
                ]);
	Route::get('detalleTarea/{id?}/{opcion?}', [  
                'uses' => 'TareasController@detalleTareas',
                'as' => 'detalleTareas'
                ]);
	Route::post('correo/enviar', [  
                'uses' => 'CorreoController@enviarCorreo',
                'as' => 'enviarCorreo'
                ]);
	Route::get('correo/cargarArchivo', [ 
                'uses' => 'CorreoController@cargarArchivo',
                'as' => 'cargarArchivo'
                ]);
		Route::get('correo/nuevoCorreo', [  
                'uses' => 'CorreoController@nuevoCorreo',
                'as' => 'nuevoCorreo'
                ]);
	Route::post('actualizar', [  
                'uses' => 'CorreoController@actualizar',
                'as' => 'actualizar'
                ]);

});

Route::get('auth/login', [
	'uses' => 'Auth\AuthController@getLogin',
	'as' => 'auth.login'
]);
Route::post('auth/login', [
	'uses' => 'Auth\AuthController@postLogin',
	'as' => 'auth.login'
]);
Route::get('auth/logout', function(){
	Auth::logout();
    return redirect()->route('auth.login');
});



