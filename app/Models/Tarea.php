<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
     protected $table= 'tareas';
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'emisor','receptor', 'asunto', 'estado', 'contenido','archivo'
    ];

}
