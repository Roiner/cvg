<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table= 'personas';
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','apellido', 'cedula', 'email', 'departamento_id',
    ];


    public function departamento(){
        return $this->belongsTo(Departamento::class,'departamento_id', 'id');
    }



}
