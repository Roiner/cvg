<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class DetalleTarea extends Model
{
   protected $table='tarea_detalles';

   protected $primaryKey='id';

public $timestamps=false;

protected $fillable=['archivo', 'tareas_id'];

// public function setArchivoAttribute($archivo)
// {
// 	$this->attributes['archivo'] = Carbon::now().$archivo;
// 	$name = Carbon::now().$archivo;
// 	\Storage::disk('local')->put($name, \File::get($archivo));
// }

}
