<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table= 'departamentos';
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','email','departamento_id'
    ];

    public function personas(){
        return $this->hasMany(Persona::class, 'departamento_id', 'id' );
    }

    public function usuarios(){
        return $this->hasMany(Persona::class, 'departamento_id', 'id' );
    }


}
