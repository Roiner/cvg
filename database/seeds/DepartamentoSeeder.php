<?php

use Illuminate\Database\Seeder;
use App\Models\Departamento;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamentos = [
        	[
        		'nombre' 				=> 'informatica',
        		'email' 			=> 'Roiner123@gmail.com',
        	]
        ];

        foreach ($departamentos as $departamento) {
			Departamento::create($departamento);        	
        }
    }
}
