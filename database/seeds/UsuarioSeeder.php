<?php

use Illuminate\Database\Seeder;
Use App\Models\Usuario;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
        	[
        		'email' 			=> 'usuario@dominio.com',
        		'password'			=> bcrypt('1234567'),
        		'tipo' 				=> 'personal',
        		'departamento_id' 	=> '1',
        	],
        	[
        		'email' 			=> 'Roiner123@gmail.com',
        		'password'			=> bcrypt('2882416'),
        		'tipo' 				=> 'administrador',
        		'departamento_id' 	=> '1',
        	]
        ];

        foreach ($usuarios as $usuario) {
			Usuario::create($usuario);        	
        }
    }
}
