{{-- <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To | SISTEMA DE GESTION Y CONTROL</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Bootstrap Core Css -->
    <link href={{ URL::asset('plugins/bootstrap/css/bootstrap.css') }} rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href={{ URL::asset('plugins/node-waves/waves.css') }} rel="stylesheet">

    <!-- Animation Css -->
    <link href={{ URL::asset('plugins/animate-css/animate.css') }} rel="stylesheet">

    <!-- Morris Chart Css-->
    <link href={{ URL::asset('plugins/morrisjs/morris.css') }} rel="stylesheet">

    <!-- JQuery DataTable Css -->
    <link href={{ URL::asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }} rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href={{ URL::asset('plugins/bootstrap-select/css/bootstrap-select.css') }} rel="stylesheet">

    <!-- Custom Css -->
    <link href={{ URL::asset('css/style.css') }} rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href={{ URL::asset('css/themes/all-themes.css') }} rel="stylesheet">

    <!-- Font Awesome Css -->
    <link href={{ URL::asset('css/font-awesome.min.css') }} rel="stylesheet">
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->

    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">SISTEMA DE GESTION Y</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <i class="fa fa-smile-o fa-3x" aria-hidden="true"></i>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="fa fa-chevron-down fa-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" aria-hidden="true"></i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="fa fa-user fa-lg" aria-hidden="true"></i>Perfil</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i>Cerrar Sesion</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Menu de Navegacion</li>
                    <li class="active">
                        <a href="index.html">
                            <i class="fa fa-home fa-2x" aria-hidden="true"></i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                            <span>Correos</span>
                        </a>
                        <ul class="ml-menu">
                        	<li>
                        		<a href="pages/widgets/cards/basic.html"> Ver Correos</a>
                        	</li>
                        	<li>
                        		<a href="pages/widgets/cards/colored.html">Enviar Correo</a>
                        	</li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                            <span>Personal</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('personas.create')}}>Nuevo Personal</a>
                            </li>
                            <li>
                                <a href={{route('personas.index')}}>Ver Personal</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-group fa-2x" aria-hidden="true"></i>
                            <span>Usuarios</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('usuarios.create')}}>Crear Usuario</a>
                            </li>
                            <li>
                                <a href={{route('usuarios.index')}}>Ver Usuarios</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                            <span>Departamentos</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('departamentos.create')}}>Crear Departamento</a>
                            </li>
                            <li>
                                <a href={{route('departamentos.index')}}>Ver Departamento</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-list-alt fa-2x" aria-hidden="true"></i>
                            <span>Listado de tareas</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('tareas.create')}}>Tareas</a>
                            </li>
                            <li>
                                <a href="pages/medias/carousel.html">Reporte de Tareas</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">CVG Fundeporte</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->

    </section>

    <section class="content">
    	<div class="container-fluid">
    		<div class="row clearfix">
    			@yield("contenido")
    		</div>
    	</div>
    </section>

    <!-- Jquery Core Js -->
    <script src={{ URL::asset('plugins/jquery/jquery.min.js') }}></script>

    <!-- Bootstrap Core Js -->
    <script src={{ URL::asset('plugins/bootstrap/js/bootstrap.js') }}></script>

    <!-- Select Plugin Js -->
    <script src={{ URL::asset('plugins/bootstrap-select/js/bootstrap-select.js') }}></script>

    <!-- Slimscroll Plugin Js -->
    <script src={{ URL::asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}></script>

    <!-- Waves Effect Plugin Js -->
    <script src={{ URL::asset('plugins/node-waves/waves.js') }}></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src={{ URL::asset('plugins/jquery-datatable/jquery.dataTables.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}></script>

    <!-- Select Plugin Js -->
    <script src={{ URL::asset('plugins/bootstrap-select/js/bootstrap-select.js') }}></script>

    <!-- Ckeditor -->
    <script src={{ URL::asset('plugins/ckeditor/ckeditor.js') }}></script>

    <!-- Custom Js -->
    <script src={{ URL::asset('js/admin.js') }}></script>
    <script src={{ URL::asset('js/pages/forms/editors.js') }}></script>

    <!-- Demo Js -->
    <script src={{ URL::asset('js/demo.js') }}></script>

    <!-- Custom Js -->
    {{-- <script src={{ URL::asset('js/admin.js') }}></script> --}}
    {{-- <script src={{ URL::asset('js/pages/tables/jquery-datatable.js') }}></script>
    <script src={{ URL::asset('js/pages/ui/modals.js') }}></script> --}}

    <!-- Demo Js -->

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | SISTEMA DE GESTION Y CONTROL</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Bootstrap Core Css -->
    <link href={{ URL::asset('plugins/bootstrap/css/bootstrap.css') }} rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href={{ URL::asset('plugins/node-waves/waves.css') }} rel="stylesheet">

    <!-- Animation Css -->
    <link href={{ URL::asset('plugins/animate-css/animate.css') }} rel="stylesheet">

    <!-- Morris Chart Css-->
    <link href={{ URL::asset('plugins/morrisjs/morris.css') }} rel="stylesheet">

    <!-- JQuery DataTable Css -->
    <link href={{ URL::asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }} rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href={{ URL::asset('plugins/bootstrap-select/css/bootstrap-select.css') }} rel="stylesheet">

     <!-- Colorpicker Css -->
     <link href={{ URL::asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }} rel="stylesheet">

    <!-- Dropzone Css -->
    <link href={{ URL::asset('plugins/dropzone/dropzone.css') }} rel="stylesheet">

    <!-- Multi Select Css -->
    <link href={{ URL::asset('plugins/multi-select/css/multi-select.css') }} rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href={{ URL::asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }} rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href={{ URL::asset('plugins/bootstrap-select/css/bootstrap-select.css') }} rel="stylesheet">

    <!-- noUISlider Css -->
    <link href={{ URL::asset('plugins/nouislider/nouislider.min.css') }} rel="stylesheet">

    <!-- Custom Css -->
    <link href={{ URL::asset('css/style.css') }} rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href={{ URL::asset('css/themes/all-themes.css') }} rel="stylesheet">

    <!-- Font Awesome Css -->
    <link href={{ URL::asset('css/font-awesome.min.css') }} rel="stylesheet">
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->

    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">SISTEMA DE GESTION Y CONTROL</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">   
                </div>
                <div class="info-container">
                    <br>
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="black">{{Auth::user()->email}}</font></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="fa fa-chevron-down fa-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" aria-hidden="true"></i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="fa fa-user fa-lg" aria-hidden="true"></i>Perfil</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href={{ url('auth/logout') }}><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i>Cerrar Sesion</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Menu de Navegacion</li>
                    <li class="active">
                        <a href="{{route('auth.login')}}"
                            <i class="fa fa-home fa-2x" aria-hidden="true"></i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                            <span>Correos</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('nuevoCorreo')}}>Enviar Correo</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                            <span>Personal</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('personas.create')}}>Nuevo Personal</a>
                            </li>
                            <li>
                                <a href={{route('personas.index')}}>Ver Personal</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-group fa-2x" aria-hidden="true"></i>
                            <span>Usuarios</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('usuarios.create')}}>Crear Usuario</a>
                            </li>
                            <li>
                                <a href={{route('usuarios.index')}}>Ver Usuarios</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                            <span>Departamentos</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('departamentos.create')}}>Crear Departamento</a>
                            </li>
                            <li>
                                <a href={{route('departamentos.index')}}>Ver Departamento</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-list-alt fa-2x" aria-hidden="true"></i>
                            <span>Listado de tareas</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href={{route('tareas.create')}}>Nueva Tareas</a>
                            </li>
                            <li>
                                <a href={{route('listaTareas',"1")}}>Lista de Tareas</a>
                            </li>
                            <li>
                                <a href={{route('listaTareas',"2")}}>Tareas Recibidas</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Fundeporte</a>.
                </div>
                
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->

        <section class="content">
            <div class="container-fluid">
                <div class="row clearfix">
                    @yield("contenido")
                </div>
            </div>
        </section>
        
    </section>

    

    <!-- Jquery Core Js -->
    <script src={{ URL::asset('plugins/jquery/jquery.min.js') }}></script>

    <!-- Bootstrap Core Js -->
    <script src={{ URL::asset('plugins/bootstrap/js/bootstrap.js') }}></script>

    <!-- Select Plugin Js -->
    <script src={{ URL::asset('plugins/bootstrap-select/js/bootstrap-select.js') }}></script>

    <!-- Slimscroll Plugin Js -->
    <script src={{ URL::asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src={{ URL::asset('plugins/jquery-datatable/jquery.dataTables.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}></script>
    <script src={{ URL::asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}></script>

    {{-- <!-- Ckeditor -->
    <script src={{ URL::asset('plugins/ckeditor/ckeditor.js') }}></script>

    <!-- TinyMCE -->
    <script src={{ URL::asset('plugins/tinymce/tinymce.js') }}></script>

    <!-- Bootstrap Colorpicker Js -->
    <script src={{ URL::asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}></script>

    <!-- Dropzone Plugin Js -->
    <script src={{ URL::asset('plugins/dropzone/dropzone.js') }}></script>

    <!-- Input Mask Plugin Js -->
    <script src={{ URL::asset('plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}></script>

    <!-- Multi Select Plugin Js -->
    <script src={{ URL::asset('plugins/multi-select/js/jquery.multi-select.js') }}></script>

    <!-- noUISlider Plugin Js -->
    <script src={{ URL::asset('plugins/nouislider/nouislider.js') }}></script> --}}

    <!-- Waves Effect Plugin Js -->
    <script src={{ URL::asset('plugins/node-waves/waves.js') }}></script>

    <!-- Custom Js -->
    <script src={{ URL::asset('js/admin.js') }}></script>
    <script src={{ URL::asset('js/pages/ui/modals.js') }}></script>
    {{-- <script src={{ URL::asset('js/pages/forms/editors.js') }}></script>
    <script src={{ URL::asset('js/pages/forms/advanced-form-elements.js') }}></script> --}}

    <!-- Demo Js -->
    <script src={{ URL::asset('js/demo.js') }}></script>
    
    <!-- Mis Script -->
    <script src={{ URL::asset('js/pasantia.js') }}></script>

    @yield("js")
    

</body>

</html>