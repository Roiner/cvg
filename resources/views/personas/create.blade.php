@extends ('template.main')
@section('title', 'Crear Empleado')
@section ('contenido')

	<!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                        	<h1>Crear Empleado</h1>
                        </div>
                        <div class="body">
                            @include('personas._form',['departamentos' => $departamentos])
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
	
@endsection