<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					Lista de Empleados
				</h2>
			</div>
			<div class="body">
				<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Cedula</th>
							<th>Email</th>
							<th>Departamento</th>
							<th>
								<a class="btn bg-teal btn-block btn-xs waves-effect" href={{route('personas.create')}}>
    								<i class="fa fa-user-plus" title="Agregar empleado"></i>
    							</a>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($personas as $persona)
							<tr>
								<td>{{$persona->nombre}}</td>
								<td>{{$persona->apellido}}</td>
								<td>{{$persona->cedula}}</td>
								<td>{{$persona->email}}</td>
								<td>{{$persona->departamento->nombre}}</td>
								<td>
									<div class="btn-toolbar">
										<div class="btn-group btn-circle">
											<a class="btn bg-orange btn-circle waves-effect waves-circle waves-float" href={{ route('personas.edit', $persona->id) }}>
    											<i class="fa fa-edit" title="Editar empleado"></i>
    										</a>
										</div>                           
										<div class="js-modal-buttons btn-group btn-circle">
											<button value={{$persona->id}} type="button" data-color='red' class=" btn eliminar bg-red btn-circle waves-effect waves-circle waves-float ">
												<i class="fa fa-user-times" title="Eliminar empleado"></i>
											</button>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
