<div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action={{ route('personas.destroy', "-1") }} method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">¿Esta seguro que desea eliminar este Empleado?</h4>
                </div>
                <div class="modal-body"> 
                    <input type="hidden" id="id-eliminar" name="id" value="">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">¡ELIMINAR!</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                </div>
            </form>
        </div>
    </div>
</div>