@if ($persona->exists)
    <form id="form-personas" class="" action={{ route('personas.update',  $persona->id) }} method="POST">
    {{ method_field('PUT') }}
@else
    <form id="form-personas" class="" action={{ route('personas.store') }} method="POST">
@endif

    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Nombre</h2>
                    <input type="text" class="form-control" name="nombre" placeholder="ej: Juan" value="{{$persona->nombre}}" />
                </div>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Apellido</h2>
                    <input type="text" class="form-control" name="apellido" placeholder="ej: Guzman" value="{{$persona->apellido}}" />
                </div>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Cedula</h2>
                    <input type="text" class="form-control" name="cedula" placeholder="ej: 12345" value="{{$persona->cedula}}"/>
                </div>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Correo</h2>
                    <input type="email" class="form-control" name="email" placeholder='ej: ejemplo@gmail.com' value="{{$persona->email}}" />
                </div>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <select class="form-control show-tick" name="departamento_id">
                        <option value="0">-- Seleccione una opcion --</option>
                        @foreach($departamentos as $departamento)
                            <option value="{{$departamento->id}}" {{ ($persona->departamento_id == $departamento->id)? 'selected':'' }}>
                                {{$departamento->nombre}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-lg btn-block bg-black waves-effect waves-light" name="">
                GUARDAR
            </button>
        </div>
    </div>
</form>