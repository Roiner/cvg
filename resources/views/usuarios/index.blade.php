@extends ('template.main')
@section('title', 'Lista de los usuarios')
@section ('contenido')

@include('usuarios._modal')
@include('usuarios._table',[ 'usuarios' => $usuarios])
	
@endsection