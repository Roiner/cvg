<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					Lista de Usuarios
				</h2>
			</div>
			<div class="body">
				<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
					<thead>
						<tr>
							<th>Correo</th>
							<th>Tipo</th>
							<th>Departamento</th>
							<th>
								<a class="btn bg-teal btn-block btn-xs waves-effect" href={{route('usuarios.create')}}>
    								<i class="fa fa-user-plus" title="Agregar usuario"></i>
    							</a>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($usuarios as $usuario)
							<tr>
								<td>{{$usuario->email}}</td>
								<td>{{$usuario->tipo}}</td>
								<td>{{$usuario->departamento->nombre}}</td>
								<td>
									<div class="btn-toolbar">
										<div class="btn-group btn-circle">
											<a class="btn bg-orange btn-circle waves-effect waves-circle waves-float" href={{ route('usuarios.edit', $usuario->id) }}>
    											<i class="fa fa-edit" title="Editar usuario"></i>
    										</a>
										</div>                           
										<div class="js-modal-buttons btn-group btn-circle">
											<button value={{$usuario->id}} type="button" data-color='red' class=" btn eliminar bg-red btn-circle waves-effect waves-circle waves-float ">
												<i class="fa fa-user-times" title="Eliminar usuario"></i>
											</button>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
