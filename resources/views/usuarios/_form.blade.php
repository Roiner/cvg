@if ($usuario->exists)
    <form id="form-usuarios" class="" action={{ route('usuarios.update',  $usuario->id) }} method="POST">
    {{ method_field('PUT') }}
@else
    <form id="form-usuarios" class="" action={{ route('usuarios.store') }} method="POST">
@endif

    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Email</h2>
                    <input type="email" class="form-control" name="email" placeholder="ej: ejemplo@gmail.com" value="{{$usuario->email}}" required />
                </div>
            </div>
            @if(!($usuario->exists))
                <div class="form-group">
                    <div class="form-line">
                        <h2 class="card-inside-title">Contraseña</h2>
                        <input type="password" class="form-control" name="password" placeholder="*********" />
                    </div>
                </div>
            @endif
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Departamento</h2>
                    <select class="form-control show-tick" name="departamento_id">
                        <option value="0">-- Seleccione una opcion --</option>
                        @foreach($departamentos as $departamento)
                            <option value="{{$departamento->id}}" {{ ($usuario->departamento_id == $departamento->id)? 'selected':'' }}>
                                {{$departamento->nombre}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Tipo</h2>
                    <select class="form-control show-tick" name="tipo">
                        <option value="0">-- Seleccione una opcion --</option>
                        <option value="administrador" {{ ($usuario->tipo == 'administrador')? 'selected':'' }}>
                            administrador
                        </option>
                        <option value="personal" {{ ($usuario->tipo == 'personal')? 'selected':'' }}>
                            personal
                        </option>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-lg btn-block bg-black waves-effect waves-light" name="">
                GUARDAR
            </button>
        </div>
    </div>
</form>