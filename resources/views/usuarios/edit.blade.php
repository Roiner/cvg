@extends ('template.main')
@section('title', 'Editar Usuario')
@section ('contenido')

	<!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                        	<h1>Editar Usuario</h1>
                        </div>
                        <div class="body">
                            @include('usuarios._form',['usuario' => $usuario, 'departamentos' => $departamentos])
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
	
@endsection