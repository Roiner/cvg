@if($opcion!=3)
<form enctype="multipart/form-data" id="form-tareas" action={{ route('actualizar') }} method="POST">
    @else
    <form enctype="multipart/form-data" id="form-tareas" action={{ route('enviarCorreo') }} method="POST">
        @endif
        {{csrf_field()}}
        <input type="hidden" name="id_tarea" value="{{$tarea->id}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Detalle de Tarea
                        </h2>
                    </div>
                    <div class="body">
                        @if($opcion==1 || $opcion==3)
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">Para</h2>
                                <input type="email" class="form-control quitar" name="receptor" placeholder="ejemplo@gmail.com" value="{{$tarea->receptor}}" required="true" />
                            </div>
                        </div>
                        @elseif($opcion==2)
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">De</h2>
                                <input type="email" class="form-control quitar" name="receptor" placeholder="ejemplo@gmail.com" value="{{$tarea->emisor}}" required  />
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">Asunto</h2>
                                <input type="text" class="form-control quitar" name="asunto" value="{{$tarea->asunto}}" required />
                            </div>
                        </div>
                        @if($opcion!=3)
                        <div class="form-group">
                            <div>
                                <h2 class="card-inside-title">Archivo</h2>
                                <label>{{$archivo}}</label>
                                <a href={{ route('descargarTarea', $tarea->archivo) }} class="btn bg-green" style="margin-left: 50px">Descargar<i class="fa fa-download" aria-hidden="true"></i></a>
                                <input type="file" class="form-control hidden" name="archivo" id="archivo"/>
                            </div>
                        </div>
                        @else
                        <div class="form-group">
                            <div>
                                <h2 class="card-inside-title">Archivo</h2>
                                <input type="file" class="form-control" name="archivo" id="archivo" />
                            </div>
                        </div>
                        @endif
                        @if($opcion==1)
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">Estado</h2>
                                <select name="estado" class="form-control quitar">
                                  <option value="en_proceso" {{ ($tarea->estado == 'en_proceso')? 'selected':'' }}>
                                      En Proceso
                                  </option>
                                  <option value="terminado" {{ ($tarea->estado == 'terminado')? 'selected':'' }}>
                                      Terminado
                                  </option>
                              </select>
                          </div>
                      </div>
                      @endif
                      <div class="form-group">
                        <div class="form-line">
                            <h2 class="card-inside-title">Contenido</h2>
                            <textarea rows="8" class="form-control quitar" name="contenido" >{{$tarea->contenido}}</textarea>
                        </div>
                    </div>
                </div>
                @if($opcion!=2)
                <div class="footer">
                    <button type="submit" class="btn btn-lg btn-block bg-blue waves-effect waves-light" name="">
                        Enviar
                    </button>
                </div>
                @endif
            </div>
        </div>
    </div>
</form>

@if($opcion == 1 || $opcion == 2)
@section('js')
<script src={{ URL::asset('js/miscript.js') }}></script>
@endsection
@endif