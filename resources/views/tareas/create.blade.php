@extends ('template.main')
@section('title', 'Crear Tarea')
@section ('contenido')

	<!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                        	<h1>Nueva Tarea</h1>
                        </div>
                        <div class="body">
                            @include('tareas._form',['tarea'=>$tarea])
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
	
@endsection