@if ($tarea->exists)
    <form id="frmFileUpload" class="dropzone" action={{ route('tareas.update',  $tarea->id) }} method="POST" enctype="multipart/form-data">
    {{ method_field('PUT') }}
@else
    <form enctype="multipart/form-data" class="dropzone" action={{ route('tareas.store') }} method="POST">
@endif
        {{csrf_field()}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Nueva Tarea
                        </h2>
                    </div>
                    <div class="body">
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">Para</h2>
                                <input type="email" class="form-control" name="receptor" placeholder="ejemplo@gmail.com" value="" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">Asunto</h2>
                                <input type="text" class="form-control" name="asunto" value="" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <input type="file" class="form-control" name="archivo" id="archivo" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <h2 class="card-inside-title">Contenido</h2>
                                <textarea rows="8" class="form-control" name="contenido"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <button type="submit" class="btn btn-lg btn-block bg-blue waves-effect waves-light" name="">
                                Enviar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# CKEditor -->


            
        </div>
    </section>
    </form>