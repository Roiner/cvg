<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					Lista de tareas
				</h2>
			</div>
			<input type="text" class="hidden" id="opcion" value={{$opcion}} name="">
			<div class="body">
				<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
					<thead>
						<tr>
							<th>Emisor</th>
							<th>Receptor</th>
							<th>Asunto</th>
							<th>
								<a class="btn bg-teal btn-block btn-xs waves-effect" href={{route('tareas.create')}}>
    								<i class="fa fa-plus" title="Agregar departamento"></i>
    							</a>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tareas as $tarea)
							<tr>
								<td>{{$tarea->emisor}}</td>
								<td>{{$tarea->receptor}}</td>
								<td>{{$tarea->asunto}}</td>
								<td>
									<div class="btn-toolbar">
										<div class="btn-group btn-circle">
											<a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" href={{ route('detalleTareas', [$tarea->id,$opcion] ) }}>
    											<i class="fa fa-info" title="Informacion de Tarea"></i>
    										</a>
										</div>                           
										@if($opcion==1)
											<div class="btn-group btn-circle">
												<a class="btn bg-green btn-circle waves-effect waves-circle waves-float" href={{ route('detalleTareas', [$tarea->id,3] ) }}>
													<i class="fa fa-send" title="Enviar Tarea Por Correo"></i>
												</a>
											</div>
											<div class="js-modal-buttons btn-group btn-circle">
												<button value={{$tarea->id}} type="button" data-color='red' class=" btn eliminar bg-red btn-circle waves-effect waves-circle waves-float ">
													<i class="fa fa-trash" title="Eliminar Tarea"></i>
												</button>
											</div>
										@endif
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


@section('js')
	<script src={{ URL::asset('js/miscript.js') }}></script>
@endsection
