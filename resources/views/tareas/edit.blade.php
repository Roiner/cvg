@extends ('template.main')
@section('title', 'Detalle de Tarea')
@section ('contenido')

<!-- Input -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
               <h1>Detalle de las tareas</h1>
           </div>
           <div class="body">
            @if($opcion==1)
                @include('tareas._form3',[ 'tarea' => $tarea, 'archivo' => $archivo])
            @else
                @include('tareas._form2',[ 'tarea' => $tarea, 'archivo' => $archivo, 'opcion' => $opcion])
            @endif
            </div>
    </div>
</div>
<!-- #END# Input -->

@endsection


