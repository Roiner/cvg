@extends ('template.main')
@section('title', 'Lista de departamentos')
@section ('contenido')

@include('departamentos._modal')
@include('departamentos._table',[ 'departamentos' => $departamentos])
	
@endsection