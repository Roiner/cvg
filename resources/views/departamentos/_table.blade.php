<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					Lista de departamentos
				</h2>
			</div>
			<div class="body">
				<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Correo</th>
							<th>
								<a class="btn bg-teal btn-block btn-xs waves-effect" href={{route('departamentos.create')}}>
    								<i class="fa fa-user-plus" title="Agregar departamento"></i>
    							</a>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($departamentos as $departamento)
							<tr>
								<td>{{$departamento->nombre}}</td>
								<td>{{$departamento->email}}</td>
								<td>
									<div class="btn-toolbar">
										<div class="btn-group btn-circle">
											<a class="btn bg-orange btn-circle waves-effect waves-circle waves-float" href={{ route('departamentos.edit', $departamento->id) }}>
    											<i class="fa fa-edit" title="Editar departamento"></i>
    										</a>
										</div>                           
										<div class="js-modal-buttons btn-group btn-circle">
											<button value={{$departamento->id}} type="button" data-color='red' class=" btn eliminar bg-red btn-circle waves-effect waves-circle waves-float ">
												<i class="fa fa-user-times" title="Eliminar departamento"></i>
											</button>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
