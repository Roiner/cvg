@if ($departamento->exists)
    <form id="form-departamentos" class="" action={{ route('departamentos.update',  $departamento->id) }} method="POST">
    {{ method_field('PUT') }}
@else
    <form id="form-departamentos" class="" action={{ route('departamentos.store') }} method="POST">
@endif

    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Nombre</h2>
                    <input type="text" class="form-control" name="nombre" placeholder="ej: deporte" value="{{$departamento->nombre}}" required />
                </div>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <h2 class="card-inside-title">Email</h2>
                    <input type="email" class="form-control" name="email" placeholder="ej: ejemplo@gmail.com" value="{{$departamento->email}}" required />
                </div>
            </div>
            <button type="submit" class="btn btn-lg btn-block bg-black waves-effect waves-light" name="">
                GUARDAR
            </button>
        </div>
    </div>
</form>